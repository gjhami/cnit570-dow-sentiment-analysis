# cnit570-DOW-sentiment-analysis

Mapping the relationship between sentiment analysis of news articles and the Dow Jones Industrial Average using linear and logistic regression.

## Description

This project uses the stocknews dataset (https://www.kaggle.com/aaron7sun/stocknews) for DOW Jones data and the top 25 reddit news articles from each day. Then, it combines these news articles and analyzes them with the flare 'en-sentiment' classifier to give them a sentiment score between -1 and 1. Linear regression is performed between the sentiment score and change in DOW as a percentage of opening price. Logistic regression is then performed, again using sentiment score as the independent variable, and using a binary version of the DOW change (0 being went down in price, 1 being went up or stayed the same) as the dependent variable. These results are obtained using the statsmodels package and graphed using matplotlib.

## How To Run

### Dependencies
This project is tested on Ubuntu 20.04 and requires apt dependencies in addition to the python packages in requirements.txt. Apt dependencies can be installed with the following command: `sudo apt update && sudo apt install python3-tk libxkbcommon-x11-0`

To install the pip package requirements for this project, run the following command from within the project directory: `pip3 install -r requirements.txt`

### Generating the Graphs
Run the program using the following command: `python3 regressions.py`
