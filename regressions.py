#!/home/g/stock-proj/.venv/bin/python3

from pty import slave_open
import flair                            # Sentiment analysis library
import pandas as pd                     # Import data frames to store and process data
from alive_progress import alive_bar    # Import library for showing progress
import numpy as np                      # Used for np.array format
import statsmodels.api as sm            # Used to compute F score of linear regression
from pathlib import Path
import matplotlib.pyplot as plt

def parse_data():
    # Date,Open,High,Low,Close,Volume,Adj Close
    # Adjusted close is used to avoid pitfalls from stock splits, etc.
    djia_df           = pd.read_csv('upload_DJIA_table.csv', usecols=['Date', 'Open', 'Adj Close'])
    djia_df['Change'] = (djia_df['Adj Close'] - djia_df['Open']) / djia_df['Open']

    # Load the flair sentiment analysis model
    sentiment_model = flair.models.TextClassifier.load('en-sentiment')

    # Load news / stocks csv's into dataframe
    # Here label = 0 if DJIA decreased and 1 if DJIA increased or stayed the same between open and close
    # Date,Label,Top1,Top2,Top3,Top4,Top5,Top6,Top7,Top8,Top9,Top10,Top11,Top12,Top13,Top14,Top15,Top16,Top17,Top18,Top19,Top20,Top21,Top22,Top23,Top24,Top25
    binary_df = pd.read_csv('Combined_News_DJIA.csv', usecols=['Date', 'Label'])

    # Date,News
    news_df     = pd.read_csv('RedditNews.csv')

    # Dates and sentiment scores: [[date, score, djia change]...]
    data_list = []

    # Get all unique dates present in both datasets
    unique_news_dates   = news_df['Date'].unique()
    unique_djia_dates   = djia_df['Date'].unique()
    unique_binary_dates = binary_df['Date'].unique()
    unique_dates        = [date for date in unique_news_dates if (date in unique_djia_dates and date in unique_binary_dates)]

    with alive_bar(total=len(unique_dates)) as bar:
        # Iterate through each date
        for date in unique_dates:
            # Get and merge all articles associated with the date
            all_articles    = news_df.loc[news_df['Date'] == date, 'News']
            merged_article  = ' '.join(all_articles.astype(str).tolist())

            # Cleanup the merged article
            cleaned_article = merged_article.replace('b\'', '')
            cleaned_article = cleaned_article.replace('b"', '')

            # Get the sentiment analysis score
            flair_article   = flair.data.Sentence(cleaned_article)
            sentiment_model.predict(flair_article)
            sentiment_score  = flair_article.labels[0].score
            if flair_article.labels[0].value == 'NEGATIVE':
                sentiment_score = sentiment_score * -1

            # Get change in djia as percentage
            djia_change = djia_df.loc[djia_df['Date'] == date, 'Change'].values[0]

            # Get change in djia as 1 or 0
            # Where 0 indicates a decrease in the djia and 1 indicates no change or increase
            binary_djia_change = binary_df.loc[binary_df['Date'] == date, 'Label'].values[0]

            # Append to data_list
            data_list.append([date, sentiment_score, djia_change, binary_djia_change])
            bar()

    # Convert resulting scores into a dataframe
    df = pd.DataFrame(data_list, columns = ['Date', 'Sentiment Score', 'DJIA Change', 'DJIA Binary'])
    df.to_csv('clean_data.csv')

# Read data with sentiment analysis from csv or generate it if it's not present
def get_data():
    path = Path('clean_data.csv')
    if not path.is_file():
        parse_data()
    df = pd.read_csv('clean_data.csv')
    return df

df = get_data()

### Linear Regression ###
# Get independent and dependent variables as series
X = df['Sentiment Score']
y = df['DJIA Change']

X       = sm.add_constant(X)    # Add a column of 1's to the array
model   = sm.OLS(y, X)          # Linear regression using OLS (ordinary least squares)
result  = model.fit()           # Fit the linear regression model to the data
print(result.summary())         # Print summary data about tthe relationship (See analysis)
fig = sm.graphics.plot_regress_exog(result, 1)
fig.show()                      # Plot the data and regression line with matplotlib

## Analysis
# For a strong relationship:
#   - F-statistic should be large, but depends on number of features
#   - Prob of f-statistic should be small (less than 0.05 for 95% confidence interval)
#   - T-value should be high
#   - Value of P>|t| should be low (less than 0.05 for 95% confidence interval)


### Logistic Regression ###
X = df[['Sentiment Score']]
y = df[['DJIA Binary']]

model = sm.Logit(y, X)          # Generate model for logistic regression
result = model.fit()            # Fit the model to the data
print(result.summary())         # Print summary statistics like z score

# Create the graph by graphing the data as a scatter plot
# Then predicting the value of many data points generated between the max and min sentiment score
# and connecting them into a line
random_sentiment    = pd.DataFrame({'Sentiment Score': np.linspace(df['Sentiment Score'].min(), df['Sentiment Score'].max(), 10000)})
pred_djia           = result.predict(random_sentiment)

plt.figure(figsize=(5, 5))  # Set figure size
ax = plt.axes()
ax.scatter(df['Sentiment Score'], df['DJIA Binary'], color='b', alpha=0.2) # Graph real data
ax.scatter(random_sentiment, pred_djia, color='black', s=4) # Graph logistic regression line
ax.set_xlabel('Sentiment Score')    # Set axis titles
ax.set_ylabel('Pr(DJIA Increase)')  # Set axis titles
plt.show()
